import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fourth-lab',
  templateUrl: './fourth-lab.component.html',
  styleUrls: ['./fourth-lab.component.css']
})
export class FourthLabComponent implements OnInit {

  n = 3;
  result;

  constructor() { }

  ngOnInit() {
  }

  substitution() {
    let substitution = [];
    let n = +this.n;

    for (let i = 0; i < n; i++){
      substitution[i] = i + 1;
    }

    // let current, k;
    for (let i = n - 1; i > 0; i--) {
      let k = this.randomInteger(n);
      console.log(k);
      let current = substitution[i];
      substitution[i] = substitution[k];
      substitution[k] = current;
      }
      console.log(substitution);
      this.result = substitution;
  }

  randomInteger(max) {
    let rand = Math.random() * (max + 1);
    return Math.floor(rand);
  }
}
