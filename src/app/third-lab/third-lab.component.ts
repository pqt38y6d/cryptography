import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-third-lab',
  templateUrl: './third-lab.component.html',
  styleUrls: ['./third-lab.component.css']
})
export class ThirdLabComponent implements OnInit {

  constructor() {
  }

  euclidResult;
  a;
  b;

  nod;
  k1;
  k2;


  ngOnInit() {
  }

  euclidAlgorithm() {

    this.a = +this.a;
    this.b = +this.b;
    while (this.a !== 0 && this.b !== 0) {
      if (this.a > this.b) {
        this.a = this.a % this.b;
      }
      else {
        this.b = this.b % this.a;
      }
    }
    this.euclidResult = this.a + this.b;
  }

  extendedEuclidAlgorithm() {
    let m = Math.max(+this.a, +this.b);
    let n = Math.min(+this.a, +this.b);

    let a = 0, a1 = 1, c = m, b = 1, b1 = 0, d = n;

    let q, r, t;
    while (true) {
      q = c / d;
      r = c % d;

      if (r != 0) {
        c = d;
        d = r;
        t = a1;
        a1 = a;
        a = t - q * a;
        t = b1;
        b1 = b;
        b = t - q * b;
      }
      else {
        break
      }
    }
    this.nod = d;
    this.k1 = -b1;
    this.k2 = -a1;
  }

  clear() {
    this.a = null;
    this.b = null;
    this.euclidResult = null;
    this.nod = null;
    this.k1 = null;
    this.k2 = null;
  }
}
