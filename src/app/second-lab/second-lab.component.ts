import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-second-lab',
  templateUrl: './second-lab.component.html',
  styleUrls: ['./second-lab.component.css']
})
export class SecondLabComponent {

  en = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
  square;
  key;
  input;
  output;

  genSqViz(lang) {
    let l = this[lang];
    this.square = [];
    for (var i = 0; i < l.length; i++) {
      this.square[i] = l.slice(i).concat(l.slice(0, i));
    }
  }

  encryption(lang, text, key) {
    if (text.length !== key.length) return false;
    this.genSqViz(lang);
    var s = "";
    for (var i = 0; i < text.length; i++) {
      if ('undefined' !== typeof this[lang].indexOf(text[i]) && 'undefined' !== typeof this[lang].indexOf(key[i])) {
        if ('undefined' !== typeof this.square[this[lang].indexOf(text[i])] &&
          'undefined' !== typeof this.square[this[lang].indexOf(text[i])][this[lang].indexOf(key[i])]) {
          s += this.square[this[lang].indexOf(text[i])][this[lang].indexOf(key[i])];
        }
      }
    }
    return s;
  }

  decryption(lang, key, cipher) {
    if (cipher.length !== key.length) return false;
    this.genSqViz(lang);
    var s = "";
    for (var i = 0; i < cipher.length; i++) {
      if ('undefined' !== typeof this[lang].indexOf(key[i])) {
        var row = this[lang].indexOf(key[i]);
        if ('undefined' !== typeof this.square[row].indexOf(cipher[i])) {
          var coll = this.square[row].indexOf(cipher[i]);
          s += this[lang][coll];
        } else {
        }
      } else {
      }
    }
    return s;
  }

  FillKeyByTextLength(params, callback) {
    let key = params.key;
    let text = params.text;
    console.log(key, text)
    const mas = Array();
    let str = '';

    let i = 0;
    let j = 0;
    while (i < text.length) {
      if (j >= key.length) {
        j = 0;
      }
      str = str + '' + key[j];
      j++;
      i++;
    }
    callback(str);
  };


  code() {
    let params;
    var alphabet = this.en || null;
    if ('undefined' !== typeof this.key && '' !== this.key &&
      'undefined' !== typeof this.input && '' !== this.input) {
      let input_mas = Array();
      let is_array = false;
      if (-1 < (this.input).indexOf(' ')) {
        input_mas = (this.input).trim().split(' ');
        is_array = true;
      } else {
        input_mas = (this.input).trim();
      }
      let replaced_input = '';
      if (true === is_array) {
        for (let i = 0; i < input_mas.length; i++) {
          params = {
            'key': this.key,
            'text': input_mas[i]
          };
          let call = this.FillKeyByTextLength(params, function (result) {
            if (0 < i) {
              replaced_input = replaced_input + ' ';
            }
            replaced_input = replaced_input + this.encryption(alphabet, input_mas[i], result);

          });
        }
      } else {
        params = {
          'key': this.key,
          'text': input_mas
        };
        let call = this.FillKeyByTextLength(params, function (result) {
          console.log(alphabet, input_mas, result);
          console.log(replaced_input);
          replaced_input = replaced_input + '' + this.encryption(alphabet, input_mas, result);

        });
      }
      this.output = replaced_input;
    }
  }

  decode() {
    let params;
    let alphabet = this.en;

    if ('undefined' !== typeof this.key && '' !== this.key &&
      'undefined' !== typeof this.output && '' !== this.output) {
      let output_mas = Array();
      let is_array = false;
      if (-1 < (this.output).indexOf(' ')) {
        output_mas = this.output.trim().split(' ');
        is_array = true;
      } else {
        output_mas = this.output.trim();
      }
      let replaced_output = '';
      if (true === is_array) {
        for (let i = 0; i < output_mas.length; i++) {
          params = {
            'key': this.key,
            'text': output_mas[i]
          };
          var call = this.FillKeyByTextLength(params, function (result) {
            if (0 < i) {
              replaced_output = replaced_output + ' ';
            }
            replaced_output = replaced_output + this.decryption(alphabet, result, output_mas[i]);
          });
        }
      } else {
        params = {
          'key': this.key,
          'text': output_mas
        };
        let call = this.FillKeyByTextLength(params, function (result) {
          replaced_output = replaced_output + '' + this.decryption(alphabet, result, output_mas);
        });
      }
    }
  }
}
