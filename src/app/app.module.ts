import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { FirstLabComponent } from './first-lab/first-lab.component';
import { SecondLabComponent } from './second-lab/second-lab.component';
import {FormsModule} from "@angular/forms";
import { ThirdLabComponent } from './third-lab/third-lab.component';
import { FourthLabComponent } from './fourth-lab/fourth-lab.component';
import { FifthLabComponent } from './fifth-lab/fifth-lab.component';
import { SixthLabComponent } from './sixth-lab/sixth-lab.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstLabComponent,
    SecondLabComponent,
    ThirdLabComponent,
    FourthLabComponent,
    FifthLabComponent,
    SixthLabComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
