import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-sixth-lab',
  templateUrl: './sixth-lab.component.html',
  styleUrls: ['./sixth-lab.component.css']
})
export class SixthLabComponent implements OnInit {

  constructor() { }

  previewUrl;
  previewUrl1;
  fileData: any

  ngOnInit() {
  }

  fileProgress(fileInput: any) {
    this.fileData = fileInput.target.files[0] as File;
    console.log('filedata: ', this.fileData);
    this.preview();
  }


  preview() {
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);

    const reader2 = new FileReader();
    reader2.readAsArrayBuffer(this.fileData);
    reader2.onload = () => {
      this.previewUrl1 = reader2.result;
    };
    // tslint:disable-next-line:variable-name
    reader.onload = () => {
      console.log(reader.result)
      this.previewUrl = reader.result;
    };
  }

  todo() {
    console.log(this.previewUrl);
    let eKey ='61DF64C9588025045C1E0A67CBF88F15';

    let encrypted = CryptoJS.AES.encrypt(this.previewUrl, eKey, {
      mode: CryptoJS.mode.CBC
    });
    let descrypted = CryptoJS.AES.decrypt(encrypted, eKey).toString(CryptoJS.enc.Utf8);
    console.log(descrypted);
    // let decrypted = CryptoJS.AES.encrypt(this.previewUrl1, eKey);
    console.log(encrypted);
    // decrypted.toString();
    // console.log(decrypted);


  }
}
