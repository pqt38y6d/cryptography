import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-fifth-lab',
  templateUrl: './fifth-lab.component.html',
  styleUrls: ['./fifth-lab.component.css']
})
export class FifthLabComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  b;
  m;
  n;
  b1;
  n1;
  m1;
  m2;
  y1;
  y2;
  z1;
  z2;

  result;



  modulePow = (b, n, m) => {
    // b = BigInt(b);
    // m = BigInt(m);
    let x = b;
    for (let i = 1; i < n; i++) {
      x *= b;
    }
    x = x % m;
    return x;
  }

  oneUser (b, n, m){
    this.result = this.modulePow(b,n,m);
  }


  clear = () => {
    this.b = null;
    this.m = null;
    this.n = null;
    this.result = null;
    this.b1 = null;
    this.n1 = null;
    this.m1 = null;
    this.m2 = null;
    this.y1 = null;
    this.y2 = null;
    this.z1 = null;
    this.z2 = null;
  }

  twoUsers(b1, n1, m1, m2) {
    this.y1 = this.modulePow(b1, n1, m1);
    this.y2 = this.modulePow(b1, n1, m2);
    this.z1 = this.modulePow(b1, this.y1, m1);
    this.z2 = this.modulePow(b1, this.y2, m2);
  }
}
