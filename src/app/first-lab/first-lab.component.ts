import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-first-lab',
  templateUrl: './first-lab.component.html',
  styleUrls: ['./first-lab.component.css']
})
export class FirstLabComponent implements OnInit {

  constructor() {

  }

  generateRandom() {
    console.clear();
    const randArray = [];
    for (let i = 0; i < 100; i++) {
      randArray.push(Math.random());
    }
    console.log(randArray);
  }


  ngOnInit() {
  }

  generateCryptoRandom() {
    console.clear();
    const randArray = [];
    for (let i = 0; i < 100; i++) {
      let [rvalue] = crypto.getRandomValues(new Uint8Array(1));
      randArray.push(rvalue);
    }
    console.log(randArray);
  }

  myGenerator() {
    console.clear();
    const randArray = [];
    const a = 45;
    const c = 21;
    const m = 67;
    let seed = 2;
    const rand = () => seed = (a * seed + c) % m;
    for (let i = 0; i < 100; i++) {
      randArray.push(rand())
    }
    console.log(randArray);

  }
}
